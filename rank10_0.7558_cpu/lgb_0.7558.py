
#加载词向量
import jieba
import gensim
import gc
import sys

TRAIN_PATH=sys.argv[1]
TEST_PATH=sys.argv[2]
VAL_PATH=sys.argv[3]
VEC_PATH=sys.argv[4]

model= gensim.models.KeyedVectors.load_word2vec_format(VEC_PATH)
keys=model.vocab.keys()
jieba.load_userdict(keys)
del keys
gc.collect()

import pandas as pd
import gc
import time
from time import  strftime

start_time=time.localtime()
str_stime=strftime("%Y_%m_%d %H:%M:%S",start_time)
print(str_stime)


train_data = pd.read_table(TRAIN_PATH,
        names= ['prefix','query_prediction','title','tag','label'], header= None, encoding='utf-8').astype(str)
val_data = pd.read_table(
    
    VAL_PATH,
        names = ['prefix','query_prediction','title','tag','label'], header = None, encoding='utf-8').astype(str)



test_data = pd.read_table(TEST_PATH,
        names = ['prefix','query_prediction','title','tag'],header = None, encoding='utf-8').astype(str)


test_data['index']=test_data.index

train_data = train_data[train_data['label'] != '音乐' ]
train_data['label'] = train_data['label'].apply(lambda x: int(x))
val_data['label'] = val_data['label'].apply(lambda x: int(x))


data=pd.concat([train_data,val_data])
data['index']=data.index
data=pd.concat([data,test_data])
print(len(data))

del train_data
del val_data
del test_data

gc.collect()

def apply_mul_core(df,multi_task):
    gc.collect()
    import multiprocessing as mlp
    num_cpu = 6
    pool = mlp.Pool(num_cpu,)
    batch_num = 1 + len(df)//num_cpu
    results = []
    for i in range(num_cpu):
        task = df[i*batch_num : (i+1)*batch_num]
        result = pool.apply_async(multi_task,(task,))
        results.append(result)
        del task
        gc.collect()
    del df
    gc.collect()
    pool.close()
    pool.join()
    res = pd.DataFrame({})
    for result in results:
        feat = result.get()
        res = pd.concat([res, feat])
        del feat
        gc.collect()
    del results
    gc.collect()
    return res

def cut_wd(x):
    s=" ".join(jieba.cut(x))
    return s



def cut_wd_multi_task(data):

    data["seg_prefix"]=data.prefix.map(lambda x:cut_wd(x))
    data['seg_title']=data.title.map(lambda x:cut_wd(x))
    data['seg_tag']=data.tag.map(lambda x:cut_wd(x))
    return data

data=apply_mul_core(data,cut_wd_multi_task)
print(len(data))
gc.collect()


import pandas as pd
import numpy as np
import gensim
from tqdm import tqdm
from scipy.stats import skew, kurtosis
from scipy.spatial.distance import cosine, cityblock, jaccard, canberra, euclidean, minkowski, braycurtis


def sent2vec(s):
    words=str(s).split()
    M=[]
    for w in words:
        try:
            M.append(model[w])
        except:
            continue
    M=np.array(M)
    v=M.sum(axis=0)
    return v/np.sqrt((v**2).sum())


def w2v_multi_task1(data):
    data['prefix_vec']=data.seg_prefix.map(lambda x:sent2vec(x))
    return  data

def w2v_multi_task2(data):
    data['title_vec']=data.seg_title.map(lambda x:sent2vec(x))
    return data

def w2v_multi_task3(data):
    data['tag_vec']=data.seg_tag.map(lambda x:sent2vec(x))
    return data

data=apply_mul_core(data,w2v_multi_task1)
print("ok")
data=apply_mul_core(data,w2v_multi_task2)
print("ok")
data=apply_mul_core(data,w2v_multi_task3)
print("OK")

gc.collect()



gc.collect()

def  distance_multi_task1(data):

    data['prefix_2_title_distance_num']=data.apply(lambda x:cosine(x.prefix_vec,x.title_vec),axis=1)
    return data
def  distance_multi_task2(data):
    data['tag_2_title_distance_num']=data.apply(lambda x:cosine(x.tag_vec,x.title_vec),axis=1)
    return data 
def  distance_multi_task3(data):
    data['prefix_2_tag_distance_num']=data.apply(lambda x:cosine(x.prefix_vec,x.tag_vec),axis=1)
    return  data

gc.collect()
data=apply_mul_core(data,distance_multi_task1)
gc.collect()
data=apply_mul_core(data,distance_multi_task2)
gc.collect()
data=apply_mul_core(data,distance_multi_task3)
gc.collect()

import Levenshtein

gc.collect()

def extract_key(pred):
    pred = eval(pred)
    pred = sorted(pred.items(), key=lambda x: x[1], reverse=True)
    key_lst=[]
    for i in range(10):
        if len(pred)<i+2:
#             pred_prob_lst.append(0)
            break
        else:
            key_lst.append(pred[i][0])
    return key_lst


def extract_value(pred):
    pred = eval(pred)
    pred = sorted(pred.items(), key=lambda x: x[1], reverse=True)
    pred_prob_lst=[]
    for i in range(10):
        if len(pred)<i+2:
#             pred_prob_lst.append(0)
            break
        else:
            pred_prob_lst.append(float(pred[i][1]))
    return pred_prob_lst

def parse_mutlti_task(data):
    data['q_key_list']=data.query_prediction.map(lambda x:extract_key(x))
    data['q_value_list']=data.query_prediction.map(lambda x:extract_value(x))
    return data
    
data=apply_mul_core(data,parse_mutlti_task)
gc.collect()



data['q_value_max_num']=data.q_value_list.map(lambda x:max(x) if len(x)>0 else np.nan)

data['q_value_mean_num']=data.q_value_list.map(lambda x:sum(x)/len(x) if len(x)>0 else np.nan)

data['q_value_sum_num']=data.q_value_list.map(lambda x:sum(x) if len(x)>0 else np.nan)


def cut_key_list(x):
    if len(x)<1:
        return x
    seg_key_list=[]
    for k in  x:
        s=" ".join(jieba.cut(k))
        seg_key_list.append(s)
        
    return seg_key_list


def cutkey_mutlti_task(data):
    data['seg_q_key_list']=data.q_key_list.map(lambda x:cut_key_list(x))
    return data

data=apply_mul_core(data,cutkey_mutlti_task)
gc.collect()

def get_distance_list(title_vec,seg_q_key_list):
    if len(seg_q_key_list)<1:
        return []
    w2v_distance_list=[]
    for k in seg_q_key_list:
        d=cosine(title_vec,sent2vec(k))
        w2v_distance_list.append(d)

    return w2v_distance_list

def w2v_dist_list_mutlti_task(data):
    data['w2v_title_q_key_distance_list']=data.apply(lambda x: get_distance_list(x.title_vec,x.seg_q_key_list),axis=1)
    return data


data=apply_mul_core(data,w2v_dist_list_mutlti_task)
gc.collect()

def get_weight_distance_list(w2v_title_q_key_distance_list,q_value_list):
    if len(w2v_title_q_key_distance_list)<1:
        return []
    s=np.array(w2v_title_q_key_distance_list)*np.array(q_value_list)
    return s

def w2v_weight_dist_list_mutlti_task(data):
    data['w2v_title_q_key_weight_distance_list']=data.apply(lambda x: get_weight_distance_list(x.w2v_title_q_key_distance_list,x.q_value_list),axis=1)
    return data

data=apply_mul_core(data,w2v_weight_dist_list_mutlti_task)
gc.collect()

data['w2v_title_key_max_distance_num']=data.w2v_title_q_key_distance_list.map(lambda x: max(x) if len(x)>0  else np.nan)
data['w2v_title_key_mean_distance_num']=data.w2v_title_q_key_distance_list.map(lambda x: sum(x)/len(x) if len(x)>0  else np.nan)
data['w2v_title_key_sum_distance_num']=data.w2v_title_q_key_distance_list.map(lambda x: sum(x) if len(x)>0  else np.nan)

data['w2v_title_key_max_weight_distance_num']=data.w2v_title_q_key_weight_distance_list.map(lambda x: max(x) if len(x)>0  else np.nan)
data['w2v_title_key_mean_weight_distance_num']=data.w2v_title_q_key_weight_distance_list.map(lambda x: sum(x)/len(x) if len(x)>0  else np.nan)
data['w2v_title_key_sum_weight_distance_num']=data.w2v_title_q_key_weight_distance_list.map(lambda x: sum(x) if len(x)>0  else np.nan)

import Levenshtein



def levenshtein_similarity(str1,str2):
    return Levenshtein.ratio(str1,str2)



def get_equal_rate(ss,q_key_list):
    if len(q_key_list)<1:
        return []
    equal_rate=[]
    for i in q_key_list:
        equal_rate.append(levenshtein_similarity(ss,i))
    return equal_rate


def get_weight_equal_rate(equal_rate_list,q_value_list):
    if len(equal_rate_list)<1:
        return []
    s=np.array(equal_rate_list)*np.array(q_value_list)
    return s

data['t_q_sm_rate_list']=data.apply(lambda x:get_equal_rate(x.title,x.q_key_list),axis=1)
data['t_q_sm_weight_rate_list']=data.apply(lambda x:get_weight_equal_rate(x.t_q_sm_rate_list,x.q_value_list),axis=1)
                          
                          
data['p_t_sm_rate_num']=data.apply(lambda x :levenshtein_similarity(x.prefix,x.title),axis=1)

data['max_t_q_sm_rate_list_num']=data.t_q_sm_rate_list.map(lambda x:max(x) if len(x)>0 else np.nan)
data['sum_t_q_sm_rate_list_num']=data.t_q_sm_rate_list.map(lambda x:sum(x) if len(x)>0 else np.nan)
data['mean_t_q_sm_rate_list_num']=data.t_q_sm_rate_list.map(lambda x:sum(x)/len(x) if len(x)>0 else np.nan)
                          
data['max_t_q_sm_weight_rate_list_num']=data.t_q_sm_weight_rate_list.map(lambda x:max(x) if len(x)>0 else np.nan)
data['sum_t_q_sm_weight_rate_list_num']=data.t_q_sm_weight_rate_list.map(lambda x:sum(x) if len(x)>0 else np.nan)
data['mean_t_q_sm_weight_rate_list_num']=data.t_q_sm_weight_rate_list.map(lambda x:sum(x)/len(x) if len(x)>0 else np.nan)


import gc
import re 
import json

data['prefix_equal_title_ca']=data.apply(lambda x: 1 if x.prefix.lower()==x.title.lower() else 0,axis=1)
data['title_in_query_ca']=data.apply(lambda x:1 if x.title.lower() in x.query_prediction.lower() else 0,axis=1)
data['prefix_in_title_head_ca']=data.apply(lambda x: 1 if x.prefix.lower()==x.title[:len(x.prefix)].lower() else 0, axis=1)
data['title_in_query_ca']=data.apply(lambda x:1 if x.title.lower() in x.query_prediction.lower() else 0,axis=1)
data['prefix_in_title_ca']=data.apply(lambda x:1 if x.prefix.lower() in x.title.lower() else 0,axis=1)


data['title_len_num']=data['title'].map(lambda x:len(x))
data['prefix_len_num']=data['prefix'].map(lambda x:len(x))
data['prefix_diff_title_len_num']=data['title_len_num']-data['prefix_len_num']
data['query_prediction_len_num']=data['query_prediction'].map(lambda x:len(str(x)))
data['prefix_dev_title_num']=data['prefix_len_num']/data['title_len_num']
data['query_prediction_dev_title_num']=data['query_prediction_len_num']/data['title_len_num']

def  get_title_value(title,query):
#     print(title,query)
    query=str(query)
    js=json.loads(query)
    try:
        value=js[title]
    except:
        value=0
    return float(value)

def  title_is_max_value(title,query):
#     print(title,query)
    query=str(query)
    js=json.loads(query)
    try:
        value=float(js[title])
    except:
        value=0
        return 0 
    values=js.values()
    values=[float(i) for i in  values]
    max_value=max(values)
    if  abs(max_value-value)<0.00001:
        return 1
    return 0



def  count_title_in_query(title,query):
#     print(title,query)
    query=str(query)
    js=json.loads(query)
    if type(js)!=dict:
        return 0

    keys=js.keys()
    count=0
    for i in keys:
        if title in i:
            count+=1
    return  count

import re 

def include_num(prefix):
    
    s=re.search("\d",str(prefix))
    if s!=None:
        return 1
    else:
        return 0

def include_zm(prefix):
    s=re.search("[a-zA-Z]",str(prefix))
    if s!=None:
        return 1
    else:
        return 0
    
def include_ch(prefix):
    s=re.search("[\u4e00-\u9fa5]+",str(prefix))
    
    if s!=None:
        return 1
    else:
        return 0
    
data['title_in_query_value_ismax_ca']=data.apply(lambda x:title_is_max_value(x.title,x.query_prediction),axis=1)
data['title_in_query_value_num']=data.apply(lambda x:get_title_value(x.title,x.query_prediction),axis=1)

data['title_in_query_count_num']=data.apply(lambda x:count_title_in_query(x.title,x.query_prediction),axis=1)
data['prefix_inc_num_ca']=data.apply(lambda x:include_num(x.prefix),axis=1)
data['prefix_inc_zm_ca']=data.apply(lambda x:include_zm(x.prefix),axis=1)
data['prefix_inc_ch_ca']=data.apply(lambda x:include_ch(x.prefix),axis=1)

def  get_keys_maxlen(query):
#     print(title,query)
    query=str(query)
    js=json.loads(query)
    if type(js)!=dict or len(js.keys())<1:
        return 0

    keys=js.keys()
    keys_len=[len(i) for i in keys]
    return max(keys_len)

def  get_keys_minlen(query):
#     print(title,query)
    query=str(query)
    js=json.loads(query)
    if type(js)!=dict or len(js.keys())<1:
        return 0

    keys=js.keys()
    keys_len=[len(i) for i in keys]
    return min(keys_len)

def  get_keys_meanlen(query):
#     print(title,query)
    query=str(query)
    js=json.loads(query)
    if type(js)!=dict or len(js.keys())<1:
        return 0

    keys=js.keys()
    keys_len=[len(i) for i in keys]
    return  sum(keys_len)/len(keys)



data['q_keys_max_len_num']=data.query_prediction.map(lambda x:get_keys_maxlen(x))
data['q_keys_min_len_num']=data.query_prediction.map(lambda x:get_keys_minlen(x))
data['q_keys_mean_len_num']=data.query_prediction.map(lambda x:get_keys_meanlen(x))

data['qkmaxl_title_num']=data['q_keys_max_len_num']-data['title_len_num']
data['qkminl_title_num']=data['title_len_num']-data['q_keys_min_len_num']
data['qkmeanl_title_num']=data['q_keys_mean_len_num']-data['title_len_num']

def feature_count(data,col1,col2):
    if col1+'_'+col2 in data.columns:
        dd = 5
    else :
        data[col1+'_'+col2] = data[col1] + data[col2]
        tf1=data.groupby([col1+'_'+col2],as_index=False)[col1+'_'+col2].agg({col1+'_'+col2+'_counts':'count'})
        data=pd.merge(data,tf1,how='left',on=[col1+'_'+col2])
    if col1+'_counts' in data.columns:
        dd = 5
    else:
        tf2=data.groupby([col1],as_index=False)[col1].agg({col1+'_counts':'count'})
        data=pd.merge(data,tf2,how='left',on=[col1])    
    if col2+'_counts' in data.columns:
        dd = 5
    else:
        tf3=data.groupby([col2],as_index=False)[col2].agg({col2+'_counts':'count'})
        data=pd.merge(data,tf3,how='left',on=[col2])
    return data 
ratio_feature_list = []
for i in ['prefix']:  
    for j in ['title','tag','query_prediction']:
        data = feature_count(data,i,j)
        data['ratio_'+j+'_of_'+i] = data[i+'_'+j+'_counts'] / data[i+'_counts']
        data['ratio_'+i+'_of_'+j] = data[i+'_'+j+'_counts'] / data[j+'_counts']
        ratio_feature_list.append('ratio_'+j+'_of_'+i)
        ratio_feature_list.append('ratio_'+i+'_of_'+j)   
for i in ['title']:  
    for j in ['tag','query_prediction']:
        data = feature_count(data,i,j)
        data['ratio_'+j+'_of_'+i] = data[i+'_'+j+'_counts'] / data[i+'_counts']
        data['ratio_'+i+'_of_'+j] = data[i+'_'+j+'_counts'] / data[j+'_counts']
        ratio_feature_list.append('ratio_'+j+'_of_'+i)
        ratio_feature_list.append('ratio_'+i+'_of_'+j) 
        
for i in ['tag']:  
    for j in ['query_prediction']:
        data = feature_count(data,i,j)
        data['ratio_'+j+'_of_'+i] = data[i+'_'+j+'_counts'] / data[i+'_counts']
        data['ratio_'+i+'_of_'+j] = data[i+'_'+j+'_counts'] / data[j+'_counts']
        ratio_feature_list.append('ratio_'+j+'_of_'+i)
        ratio_feature_list.append('ratio_'+i+'_of_'+j) 

temp1=data.groupby(['prefix','title']).agg('size').reset_index().rename(columns={\
                                                                    0:'pt_count'})

temp2=data.groupby(['prefix']).agg('size').reset_index().rename(columns={\
                                                                    0:'prefix_count'})
temp=pd.merge(temp1,temp2,how='left',on=['prefix'])
temp['t_in_p_bgl_rate_num']=(temp['pt_count'])/(temp['prefix_count'])
data=pd.merge(data,temp,how='left',on=['prefix','title'])


temp1=data.groupby(['prefix','title','tag']).agg('size').reset_index().rename(columns={\
                                                                    0:'ptg_count_num'})

temp2=data.groupby(['prefix']).agg('size').reset_index().rename(columns={\
                                                                    0:'prefix_count'})
temp=pd.merge(temp1,temp2,how='left',on=['prefix'])
temp['tg_in_p_bgl_rate_num']=(temp['ptg_count_num'])/(temp['prefix_count'])
data=pd.merge(data,temp,how='left',on=['prefix','title','tag'])


temp1=data.groupby(['prefix','tag']).agg('size').reset_index().rename(columns={\
                                                                    0:'pg_count'})

temp2=data.groupby(['prefix']).agg('size').reset_index().rename(columns={\
                                                                    0:'prefix_count'})
temp=pd.merge(temp1,temp2,how='left',on=['prefix'])
temp['g_in_p_bgl_rate_num']=(temp['pg_count'])/(temp['prefix_count'])
data=pd.merge(data,temp,how='left',on=['prefix','tag'])

temp1=data.groupby(['prefix','query_prediction','tag','title']).agg('size').reset_index().rename(columns={\
                                                                    0:'pqtt_count'})

temp2=data.groupby(['prefix']).agg('size').reset_index().rename(columns={\
                                                                    0:'prefix_count'})
temp=pd.merge(temp1,temp2,how='left',on=['prefix'])
temp['qtt_in_p_bgl_rate_num']=(temp['pqtt_count'])/(temp['prefix_count'])
data=pd.merge(data,temp,how='left',on=['prefix','query_prediction','tag','title'])


# nunique

fea_list=['tag','query_prediction','title']

for fea in fea_list:
    gp1=data.groupby('prefix')[fea].nunique().reset_index().rename(columns={fea:"prefix_%s_nuq_num"%fea})
    gp2=data.groupby(fea)['prefix'].nunique().reset_index().rename(columns={'prefix':"%s_prefix_nuq_num"%fea})
    data=pd.merge(data,gp1,how='left',on=['prefix'])
    data=pd.merge(data,gp2,how='left',on=[fea])   
    gc.collect()
    
fea_list=['tag','query_prediction'] 

for fea in fea_list:
    gp1=data.groupby('title')[fea].nunique().reset_index().rename(columns={fea:"title_%s_nuq_num"%fea})
    gp2=data.groupby(fea)['title'].nunique().reset_index().rename(columns={'title':"%s_title_nuq_num"%fea})
    data=pd.merge(data,gp1,how='left',on=['title'])
    data=pd.merge(data,gp2,how='left',on=[fea])   
    gc.collect()
    


fea_list=[['tag','title']]

for fea in fea_list:
    gp2=data.groupby(fea)['prefix'].nunique().reset_index().rename(columns={'prefix':"%s_prefix_nuq_num"%\
                                                                                  "_".join(fea)})
    data=pd.merge(data,gp2,how='left',on=fea)   
    gc.collect()
    



items = ['tag','prefix','query_prediction','title']

for item in items:
    temp = data.groupby(item).agg('size').reset_index().rename(columns={0:'%s_count_num'%item})
    data = pd.merge(data, temp, on=item, how='left')
    data['%s_count_rate_num'%item]=data['%s_count_num'%item]/len(data)

# data.label.value_counts()

from tqdm import tqdm_notebook
origin_cate_list=['prefix','query_prediction', 'title', 'tag']


for i in tqdm_notebook(origin_cate_list):

    data[i] = data[i].map(dict(zip(data[i].unique(), range(0, data[i].nunique()))))
    

end_time=time.localtime()
str_stime=strftime("%Y_%m_%d %H:%M:%S",end_time)
print(str_stime)

train_data=data[data.label.notna()]# 
test_data=data[data.label.isna()]
print(len(train_data),len(test_data))

train_data.sort_values(by='index',inplace=True)

test_data.sort_values(by='index',inplace=True)

feature=[i for i in data.columns if  "_distance_num"  in i ]
print(feature,len(feature))

# train_data[feature].to_csv("w2v_train_fea.csv",index=False)
# test_data[feature].to_csv("w2v_test_fea.csv",index=False)


def get_data_ctr_fea(tj_data,self_data):

    items = ['prefix', 'title', 'tag',['prefix','title','tag']]

    for item in items:
        temp = tj_data.groupby(item, as_index = False)['label'].agg({"_".join(item)+'_click':'sum', "_".join(item)+'_count':'count'})
        temp["_".join(item)+'_ctr'] =100* (temp["_".join(item)+'_click']+0.01)/(temp["_".join(item)+'_count']+0.01)
        self_data = pd.merge(self_data, temp, on=item, how='left')
        
    items = ['prefix', 'title', 'tag']
    for i in range(len(items)):
        for j in range(i+1, len(items)):
            item_g = [items[i], items[j]]
            temp = tj_data.groupby(item_g, as_index=False)['label'].agg({'_'.join(item_g)+'_click': 'sum','_'.join(item_g)+'count':'count'})
            temp['_'.join(item_g)+'_ctr'] =100* (temp['_'.join(item_g)+'_click']+0.01)/(temp['_'.join(item_g)+'count']+0.01)
            self_data = pd.merge(self_data, temp, on=item_g, how='left')
            
    return self_data
            
test_data=get_data_ctr_fea(train_data,test_data)

from scipy import sparse

origin_cate_list=['prefix','title','tag','query_prediction']

num_feature=[i for i in  test_data.columns if '_num' in i  or'ratio' in i or"_ctr" in i]
ca_fea=origin_cate_list+[i for i in  test_data.columns if '_ca' in i ]
num_feature+=ca_fea

feature=num_feature
print(feature,len(feature))
# train_x=train_data[feature].values
# test_x=predict_data[feature].values
# # val_x=val_data[feature].values
# print(train_x.shape,test_x.shape)
# print(train_y.shape)
# print(len(feature))

import gc


gc.collect()

import lightgbm as lgb
from sklearn.model_selection import  StratifiedKFold
from sklearn.metrics import  f1_score
import numpy as np
import logging


from sklearn.metrics import f1_score


logging.basicConfig(filename='lgb_log.log', level=logging.INFO)

def lgb_f1_score(y_hat, data):
    y_true = data.get_label()
    y_hat = np.round(y_hat) # scikits f1 doesn't like probabilities
    return 'f1', f1_score(y_true, y_hat), True

NFOLD=5

test_x=test_data[feature].values

# oof_train=np.zeros((train_x.shape[0],1))
oof_test=np.zeros((test_x.shape[0],1))
oof_test_skf=np.zeros((NFOLD,test_x.shape[0],1))


# X=train_x
# y=train_y
xx_logloss = []
xx_submit = []
cv_f1_csv=[]
val_f1_csv=[]

skf = StratifiedKFold(n_splits=NFOLD, random_state=2018, shuffle=True)

params={"booster":'gdbt','objective':'binary','max_depth':-1,'metric':'binary_logloss',
            'lambda_l1':0,'lambda_l2':1,
#             
            'num_leave':31,'max_bin':250,'min_data_in_leaf': 200,'learning_rate': 0.05,'feature_fraction': 0.8,
            
            'bagging_fraction': 0.7,'bagging_freq': 1,'nthread':12}


for k, (train_in, test_in) in enumerate(skf.split(train_data.index.values.reshape(-1,1), train_data.label.values)):
    train_df=train_data[train_data.index.isin(train_in)]
    val_df=train_data[train_data.index.isin(test_in)]
    
    train_df=get_data_ctr_fea(val_df,train_df)
    val_df=get_data_ctr_fea(train_df,val_df)
    
    train_x=train_df[feature].values
    train_y=train_df.label.values
    
    val_x=val_df[feature].values
    val_y=val_df.label.values

#     X_train, X_test, y_train, y_test = X[train_in], X[test_in], y[train_in], y[test_in]

    lgb_train = lgb.Dataset(train_x, train_y,feature_name=feature)
    lgb_eval = lgb.Dataset(val_x,val_y,feature_name=feature)

    gbm = lgb.train(params,
                    lgb_train,
                    num_boost_round=100000,
                    valid_sets=(lgb_train,lgb_eval),
                    early_stopping_rounds=100,
                    )
    
    PROB_THR=0.39
    
    f1=f1_score(val_y, (gbm.predict(val_x, num_iteration=gbm.best_iteration).reshape(-1,1)>PROB_THR).astype(int).reshape(-1))
    print("cv f1 %s"%f1)
    logging.info("第 %d折 cv f1 is %s"%(k,f1))
    cv_f1_csv.append(f1)

    predict=gbm.predict(test_x, num_iteration=gbm.best_iteration).reshape(-1,1)
    oof_test_skf[k:,]=predict
    gc.collect()

print('mean cv f1 score:',np.mean(cv_f1_csv))



# print(len(feature))
# fea_imp=pd.DataFrame({'column': feature,'importance': gbm.feature_importance()}).sort_values(by='importance',ascending=False)
# fea_imp.head(200)

oof_test=oof_test_skf.mean(axis=0)
test_data['label']=(oof_test> PROB_THR).astype(int).reshape(-1)
test_data.sort_values(by=['index'],inplace=True)

print("输出csv结果")

test_data['label'].astype(int).to_csv("/result.csv",index=False)

print(test_data.label.mean())

# print('mean cv f1 score:',np.mean(cv_f1_csv)
print(cv_f1_csv)
