a.解体思路：从ctr问题上面来思考主要是提取了perfix,tag,title及其相互组合的点击率特征，从文本匹配的思路来做主要是利用腾讯词向量来计算prefix,tag,title相互间的空间相似度以及levenshtein字符间的相似度,然后加上一些统计特征输入lgb模型训练预测。

b.数据读取和点击率这块参考了小幸运同学在知乎上面开源的代码：https://zhuanlan.zhihu.com/p/46482521A   levenshtein字符间相似度参考了github上面的开源代码：https://github.com/GrinAndBear/OGeek/tree/1fad9ebff5c9946424a12820a0ab1affb536bbe8

c.预估训练+预测 大概需要两个小时

g.第三方词向量: 腾讯开源词向量名称：Tencent_AILab_ChineseEmbedding.tx 链接：https://ai.tencent.com/ailab/nlp/data/Tencent_AILab_ChineseEmbedding.tar.gz

h.队伍名称：爱拼才会赢@   队长：邓勇，曾获得第三届阿里云安全算法挑战赛top10,达观杯文本智能处理大赛top2。 联系方式：18518490753
